package pl.margor.service;

import org.springframework.stereotype.Service;

@Service
public class HelloService {
    
    public String getHelloMsg() {
        return "Hello World";
    }
    
}
