package pl.margor.hello;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import pl.margor.service.HelloService;
import pl.margor.springutils.Redirector;

@Component("testBean")
@Scope("view")
@RequiredArgsConstructor
public class TestBean implements Serializable {

    private final HelloService helloSrv;
    private final Redirector redirector;
    @Getter private int counter;

    @PostConstruct
    private void init() {
        counter = 1;
    }

    /*
     * actions
     */
    public void inc() {
        counter++;
    }

    public void nextPage() throws IOException {
        redirector.redirect("nextpage.jsf");
    }

    public void prevPage() throws IOException {
        redirector.redirect("index.jsf");
    }

    public String getSrvMsg() {
        return helloSrv.getHelloMsg();
    }

}
